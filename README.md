# PTB Course 2016

This README will be expanded on in the following weeks, but for today (12-04) we
will only do the following things:
- Get Octave/Matlab running
- Get Psychtoolbox downloaded and installed
- Get the following command to work: `Screen('OpenWindow', 0, [], [0, 0, 300, 300]`
- Know about/how-to-do:
  - For loop
  - While loop
  - If statements
  - Switch statements
  - Comments
  - Vector indexing
- Look at the Styleguide
