# Coding guidelines

When writing code, it is important to make your code both legible and
comprehensible. This not only makes life easier for me (when I have to check
your assignments), it also makes life easier for you. When you come back in 3
months time and look at your code, you will be happy that you took the time to
make your code clear.

# Syntax guidelines
## Indentation:
- Indent anything inside a loop or if-statement
- Indent anything inside a function that isn’t the top level function
- You may indent everything inside the top-level function

## Whitespace
- Use whitespace around operators and after comma’s
- Sometimes whitespace can be omitted for clarity

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.{matlab}
result=3*x+y^7-3+x*2;                 % Bad
result = 3 * x + y ^ 7 - 3 + x * 2;   % Better
result = 3*x + y^7 - 3 + x*2;         % Best

foo = repmat ([1,2,3,4],2,1);         % Bad
foo = repmat([1, 2, 3, 4], 2, 1);     % Good
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Comments
- Comments are very important, use them enough, but not too much
- Comments are good when they explain something non-obvious

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.{matlab}
function my_imgs = comments_example
%function comments_example
%
% This function is just an example of a decent way to use comments. Try to
% always but a small comment right underneath each function you write that
% explains what the function does. The comment immediatly under a function
% is what is displayed when you call "help my_function" from the prompt.
%

N = 10;    % BAD: ’set N to 10’, GOOD: ’set amount of images generated’
f = @sin;
img_size = 100;
freqs = linspace(0, 20, N);

% BAD: make a meshgrid, GOOD: ’Create a grid of x-coordinates
axs = meshgrid(linspace(-pi, pi, img_size));
my_imgs = cell(N, 1);

%BAD: ’loop from 1 to N’ GOOD: ’Create an image for each frequency’
for i = 1 : N
    my_imgs{i} = f(freqs(i) * axs);
end
end
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Line length

- Try to keep your lines under 80 characters wide, unless it makes sense to go
over this limit.
- It is almost never right to put multiple statements on 1 line
- Don’t make 1 line if-statements or for loops

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.{matlab}
% Don’t do:
result = meshgrid(linspace(0, sin(pi * exp(pi) - size(magic(5), 2)), 300 * sqrt(1234) - 600 / mean(rand(length(linspace(0, 100, 100))))));
% But rather:
result = meshgrid(linspace(0, sin(pi * exp(pi) - size(magic(5), 2)), ...
                  300 * sqrt(1234) - 600 / ...
                  mean(rand(length(linspace(0, 100 100))))));

% Don’t do:
x = 1; y = 2; z = 3;
% But rather
x = 1;
y = 2;
z = 3;

% Don’t do:
if mod(3, 2) == 0, fprintf(’Even\n’); else, fprintf(’False\n’); end
% But rather:
if mod(3, 2) == 0
    fprintf(’Even\n’);
else
    fprintf(’False\n’);
end
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Variable names
- Try to use good variable names that describe what the variable is doing
- Sometimes names like ’i’, ’j’ in loops is fine
- Names for ’a’, ’b’ for variables in a mathematical function are fine
- Either use: VariableName, or variable_name , but be consistent
