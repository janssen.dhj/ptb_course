function trial = prompt_trial()
  trial.target = input('Target color: ');
  trial.other = input('Other color: ');
  trial.interval = input('Signal interval: ');
  trial.side = input('Signal side (l/r): ', 's');
end
