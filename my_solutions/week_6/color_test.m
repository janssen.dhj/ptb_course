function trials = color_test()

  %% Set up the parameters of the experiment

  %exp.target = input('Target color: ');
  %exp.backg = input('Other color: ');
  %exp.n_trials = input('How many trials: ');

  exp.target = [0, 0, 0];
  exp.backg = [255, 0 255];
  exp.n_trials = 10;

  [lrect, rrect] = get_rects(300, 300, 0);
  bg_color = [100, 100, 100];

  ITI = 1;
  ISI = .5;
  SI = .2;
  RI = 2;

  BUTTON_0 = 39; % 'a'
  BUTTON_1 = 40; % 's'

  res = Screen('Resolution', 0);

  %% Open the window and the audioport
  win = Screen('OpenWindow', 0, bg_color);
  aud = PsychPortAudio('Open', [], [], 0, 44100, 1);
  dur = 0.2
  t = linspace(0, 2*pi*dur, 44100 * dur);
  low = sin(t * 200);
  mid = sin(t * 400);
  high = sin(t * 800);

  KbQueueCreate();


  %% Run the trials

  trials = struct('target', [],
                  'response', [],
                  'was_left', []);

  Screen('DrawText', win, "Please push any button when you are ready to begin. Press a for first interval. Press s for second interval",
        300, 300);
  Screen('Flip', win);
  KbWait();

  try

    for i = 1 : exp.n_trials
      trials(i).target = rand() > 0.5;
      trials(i).was_left = rand() > 0.5;

      % Set the colors of the squares;
      l1 = exp.backg;
      r1 = exp.backg;
      l2 = exp.backg;
      r2 = exp.backg;

      if trials(i).target == 0
        if trials(i).was_left
          l1 = exp.target;
        else
          r1 = exp.target;
        end
      else
        if trials(i).was_left
          l2 = exp.target;
        else
          r2 = exp.target;
        end
      end


      %% ITI
      Screen('DrawLine', win, [], res.width / 2 - 30, res.height/ 2,
                                  res.width / 2 + 30, res.height/ 2);
      Screen('DrawLine', win, [], res.width / 2, res.height/ 2 - 30,
                                  res.width / 2, res.height/ 2 + 30);
      Screen('Flip', win);
      WaitSecs(ITI);

      %% SI 1
      play(aud, low);
      Screen('DrawLine', win, [], res.width / 2 - 30, res.height/ 2,
              res.width / 2 + 30, res.height/ 2);
      Screen('DrawLine', win, [], res.width / 2, res.height/ 2 - 30,
             res.width / 2, res.height/ 2 + 30);
      Screen('FillRect', win, l1, lrect);
      Screen('FillRect', win, r1, rrect);
      Screen('Flip', win);
      WaitSecs(SI);
      play(aud, mid);

      %% ISI
      Screen('DrawLine', win, [], res.width / 2 - 30, res.height/ 2,
             res.width / 2 + 30, res.height/ 2);
      Screen('DrawLine', win, [], res.width / 2, res.height/ 2 - 30,
             res.width / 2, res.height/ 2 + 30);


      Screen('Flip', win);
      WaitSecs(ISI);

      %% SI 2
      Screen('DrawLine', win, [], res.width / 2 - 30, res.height/ 2,
             res.width / 2 + 30, res.height/ 2);
      Screen('DrawLine', win, [], res.width / 2, res.height/ 2 - 30,
             res.width / 2, res.height/ 2 + 30);

      play(aud, high);
      Screen('FillRect', win, l2, lrect);
      Screen('FillRect', win, r2, rrect);
      Screen('Flip', win);
      WaitSecs(SI);
      play(aud, mid);

      %% Response
      Screen('Flip', win);
      KbQueueStart();
      WaitSecs(RI);

      %% Play feedback sound
      if trials(i).target == 0
        play(aud, low);
      else
        play(aud, high);
      end

      [_, codes] = KbQueueCheck();
      KbQueueStop();
      KbQueueFlush();

      %% Register the response
      codes = find(codes);
      if length(codes) == 1 && codes == BUTTON_0
        trials(i).response = 0;
      elseif length(codes) == 1 && codes == BUTTON_1
        trials(i).response = 1;
      else
        trials(i).response = -1;
      end
    end

  catch e
    sca();
    PsychPortAudio('Close', aud);
    KbQueueRelease();
    rethrow(e)
 end


  sca();
  PsychPortAudio('Close', aud);
  KbQueueRelease();
end

function play(aud, sound)
  PsychPortAudio('FillBuffer', aud, sound);
  PsychPortAudio('Start', aud);
end
