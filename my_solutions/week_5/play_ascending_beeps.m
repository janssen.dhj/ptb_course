function play_ascending_beeps()

  N_CHANNELS = 1;
  FREQUENCY = 44100;
  DURATION = 1;
  PAUSE = 0.2;

  freqs = linspace(300, 1000, 5);

  %% Open the audioport
  aud = PsychPortAudio('Open', [], [], 0, FREQUENCY, N_CHANNELS);

  t = linspace(0, DURATION * 2 * pi, DURATION * FREQUENCY);

  for f = freqs
    wave = sin(f * t);
    wave = taper_wave(wave, 500);
    PsychPortAudio('FillBuffer', aud, wave);

    t1 = PsychPortAudio('Start', aud);
    t2 = PsychPortAudio('Stop', aud, DURATION);

    WaitSecs(PAUSE);

  end

  PsychPortAudio('Close', aud);
end
