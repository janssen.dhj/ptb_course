function new = taper_wave(wav, n)

  if nargin < 2
    n = 50;
  end

  slope = hanning(n * 2)';
  window = [slope(1:n) ones(1, length(wav) - 2 * n) slope(n+1:end)];
  new = window .* wav;

end
