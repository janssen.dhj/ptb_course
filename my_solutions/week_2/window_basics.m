function window_basics
Screen('Preference', 'SkipSyncTests', 1);

sz = [200, 200];
h1 = Screen('OpenWindow', 0, [], make_rect([0, 0], sz));
h2 = Screen('OpenWindow', 0, [], make_rect([1400, 0], sz));
h3 = Screen('OpenWindow', 0, [], make_rect([1400, 700], sz));
h4 = Screen('OpenWindow', 0, [],make_rect([0, 700], sz));

Screen('Close', h4);
pause(1);
Screen('Close', h3);
pause(1);
Screen('Close', h2);
pause(1);
Screen('Close', h1);
