function colorful_squares

  %% Setup the parameters
  WIN_POS = [0, 0];      % Left/top corner of window
  WIN_SIZE = [512, 512];   % Width/height of window
  N_BLOCK = [8, 8];        % How many squares to divide window

  %% Generate the window and calculate derivative values
  block_size = WIN_SIZE ./ N_BLOCK;     % Size of 1 block in pixels
  [h, rect] = Screen('OpenWindow', 0, [], make_rect(WIN_POS, WIN_SIZE));

  %% Run the frames
  for f = 0 : 255
    %% Draw all the filled rects
    for i = 0 : (N_BLOCK(1) - 1)
      l = i * block_size(1);
      for j = 0 : (N_BLOCK(2) - 1)
        t = j * block_size(2);
        Screen('FillRect', h, [i * 32, j * 32, f], make_rect([l, t], block_size));
      end
    end

    Screen('Flip', h);
    pause(0.025)
  end

  %% Show the drawing, wait a bit, close
  Screen('Close', h);

end

function rect = make_rect(lt, wh)
  rect = [lt, lt + wh];
end
