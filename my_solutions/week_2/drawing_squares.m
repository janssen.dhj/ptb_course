function drawing_squares

  %% Setup the parameters
  WIN_POS = [0, 0];      % Left/top corner of window
  WIN_SIZE = [512, 512];   % Width/height of window
  N_BLOCK = [8, 8];        % How many squares to divide window

  %% Generate the window and calculate derivative values
  block_size = WIN_SIZE ./ N_BLOCK;     % Size of 1 block in pixels
  color_inc = 255 / prod(N_BLOCK);      % Size of 1 'colorstep' between blocks
  [h, rect] = Screen('OpenWindow', 0, [], make_rect(WIN_POS, WIN_SIZE));

  %% Draw all the filled rects
  for i = 0 : (N_BLOCK(1) - 1)
    l = i * block_size(1);
    for j = 0 : (N_BLOCK(2) - 1)
      n = (i * N_BLOCK(2)) + j + 1;
      t = j * block_size(2);
      Screen('FillRect', h, color_inc * n, make_rect([l, t], block_size));
    end
  end

  %% Show the drawing, wait a bit, close
  Screen('Flip', h);
  pause(5);
  Screen('Close', h);

end


function rect = make_rect(lt, wh)
  rect = [lt, lt + wh];
end
