function push_space
  while 1
    WaitSecs(0.1);
    printf("Please push the spacebar.\n")
    fflush(stdout);
    [t, code] = KbWait();
    if find(code) == [66]
      break;
    end
    printf("%s is not Space. ", KbName(find(code)));
  end

  printf("You found the spacebar!\n")

end
