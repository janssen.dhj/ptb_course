function check_buttons

  KbQueueCreate();
  KbQueueStart();
  WaitSecs(2);
  KbQueueStop();
  [a, b, c, d, e] = KbQueueCheck();
  idcs = find(b ~= 0);

  for idx = idcs
    disp(KbName(idx))
  end

end
