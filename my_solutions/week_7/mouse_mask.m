function mouse_mask

  scr = 0;
  bg = GrayIndex(scr);

  img_w = 800;
  img_h = 800;
  img = rand(img_w, img_h) * WhiteIndex(scr);

  msize = 50;
  mfull = msize * 2 + 1;
  mask = zeros(mfull, mfull, 2);
  mask(:, :, 1) = bg;
  mask(:, :, 2) = hanning(mfull) * hanning(mfull)' * 255;

  [win, wrect] = Screen('OpenWindow', scr, bg, [0 0 img_w img_h]);
  img_tex = Screen('MakeTexture', win, img);
  mask_tex = Screen('MakeTexture', win, mask);

  KbQueueCreate();
  KbQueueStart();

  [a, b] = WindowCenter(win);
  SetMouse(a, b, scr);
  HideCursor();

  while 1

    WaitSecs(0.01);

    [x, y] = GetMouse(scr);
    drect = [x - msize, y - msize, x + msize + 1, y + msize + 1];

    Screen('DrawTexture', win, img_tex, drect, drect);
    Screen('BlendFunction', win, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
    Screen('DrawTexture', win, mask_tex, [], drect);
    Screen('Flip', win);
    Screen('BlendFunction', win, GL_ONE, GL_ZERO);


    [_, keys] = KbQueueCheck();
    if any(find(keys) == KbName('q'))
      break
    end
  end

  sca;
  clear all;
  ShowCursor();
end
