function fading_texture

  N = 512;
  freq = 8;
  x = linspace(-pi, pi, N);
  [x, _] = meshgrid(x, x);
  mask = hanning(N) * hanning(N)';
  y = (sin(freq * x) .* mask + 1) * 255 / 2;
  cs = hanning(600)';

  win = Screen('OpenWindow', 0, [255 255 255] / 2, [0, 0, N, N]);
  tex = Screen('MakeTexture', win, y);
  Screen('BlendFunction', win, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  for c = cs
    Screen('DrawTexture', win, tex, [], [], [], [], c);
    Screen('Flip', win);
  end

  sca
end
