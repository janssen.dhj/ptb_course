function color_test()

  trial = prompt_trial();

  try
    run(trial);
  catch e
    Screen('CloseAll');
    rethrow(e);
  end
end
