function color_test_many()

  target = input('Target color: ');
  other = input('Other color: ');
  n = input('Amount of trials: ');

  for i = 1 : n
    trials(i).target = target;
    trials(i).other = other;
    if rand() > 0.5
      trials(i).interval = 1;
    else
      trials(i).interval = 2;
    end

    if rand() > 0.5
      trials(i).side = 'l';
    else
      trials(i).side = 'r';
    end
  end

  try
    run(trials);
  catch e
    Screen('CloseAll');
    rethrow(e);
  end

end
