function run(trials)

  WIN_ID = 0;
  [LRECT, RRECT] = get_rects(400, 400, WIN_ID);

  h = Screen('OpenWindow', WIN_ID);

  for i = 1 : length(trials)
    %% ISI
    Screen('Flip', h)
    WaitSecs(2);

    %% Interval 1
    [lc, rc] = get_colors(trials(i), 1);
    Screen('FillRect', h, lc, LRECT);
    Screen('FillRect', h, rc, RRECT);
    Screen('Flip', h);
    WaitSecs(0.5);

    %% ITI
    Screen('Flip', h);
    WaitSecs(1);

    %% Interval 2
    [lc, rc] = get_colors(trials(i), 2);
    Screen('FillRect', h, lc, LRECT);
    Screen('FillRect', h, rc, RRECT);
    Screen('Flip', h);
    WaitSecs(0.5);

    %% Response interval
    Screen('Flip', h);
    WaitSecs(1);
    %% Usually more would happen here, but we will look at capturing responses
    %% at some later time.

  end
  Screen('CloseAll');
end


function [l_color, r_color] = get_colors(trial, interval)
  %% Return the colors for left and right for a given interval
  if trial.interval == interval;
    if strcmp(trial.side, 'l')
      l_color = trial.target;
      r_color = trial.other;
    else
      l_color = trial.other;
      r_color = trial.target;
    end
  else
    l_color = trial.other;
    r_color = trial.other;
  end
end
