function [lrect, rrect] = get_rects(w, h, screen_id)
% Return the rects of a given size centered in the left and right half of the
% screen

  if nargin < 3
    screen_id = 0;
  end

  sc = Screen('Resolution', screen_id);
  lm = [sc.width / 4, sc.height / 2];
  rm = [3 * sc.width / 4, sc.height / 2];

  w = w / 2;
  h = h / 2;

  lrect = [lm(1) - w, lm(2) - h, lm(1) + w, lm(2) + h];
  rrect = [rm(1) - w, rm(2) - h, rm(1) + w, rm(2) + h];
end
