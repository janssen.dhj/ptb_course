function show_image(fname, duration, bg_color)

  % Setup default arguments
  if nargin < 2
    duration = 1;
  end

  if nargin < 3
    bg_color = [0, 0, 255];
  end

  % Load the image
  img = imread(fname);

  % Run the 'experiment'
  try
    run(img, duration, bg_color);
  catch e
    Screen('CloseAll');
    rethrow(e);
  end
end


function run(img, duration, bg_color)
  h = Screen('OpenWindow', 0, bg_color);
  tex = Screen('MakeTexture', h, img);
  Screen('DrawTexture', h, tex);
  Screen('Flip', h);

  pause(duration);
  Screen('CloseAll');
end
