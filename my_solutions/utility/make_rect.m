function rect = make_rect(lt, wh)
%%% Return a PtB rect from a 'left-top' and 'width-height' pair
  rect = [lt, lt + wh];
end
