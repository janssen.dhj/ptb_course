function while_loop
%function while_loop
%
% A demonstration of while-loops

    %% A basic while-loop
    % A while-loop is a little bit like a mix between a for-loop and an
    % if-statement. Like a for-loop it will execute a certain statement
    % over-and-over again. However, a for-loop has a fixed amount of times
    % it does this, for example:
    %
    % for i = 1 : 10
    %     do something;
    % end
    % 
    % will do something exactly 10 times.
    %
    % A while-loop is like an if-statement because it's behavior depends on
    % the true-or-false value of a statement. It will keep looping while its
    % statement is true, and the moment the statement becomes false, it will
    % stop.
    %
    % The statement of a while loop is:
    % while SOME_TEST
    %    do something;
    % end
    
    next_section('counting in a while-loop')
    % We make a while-loop that counts to 10:
    i = 1;
    while i <= 10
        fprintf('i is now: %d\n', i);
        i = i + 1;
    end
    
    % We make a while-loop that brings the numbers a and b to become equal:
    next_section('making two numbers equal')
    a = 15;
    b = 0;
    while a > b
        fprintf('a is: %2d\tb is: %d\n', a, b);
        a = a - 1;
        b = b + 1;
    end
    
    %% Infinite while-loops
    % Sometimes we want to have an infinite loop, i.e., a loop that never
    % exits until you explicitly tell it to. We can do this by using a
    % 'while true' loop. 'true' is always true, so the while-loop will
    % continue forever and ever and ever. We can still break out of a while
    % loop but using the 'break' command. When you say 'break' in a while
    % loop (or in a for-loop) the loop is immediately left, and the rest of
    % the program continues.
    %
    
    next_section('rolling a 100-sided dice untill you get 100');
    fprintf('Going to roll a dice a lot in 3 seconds\n');
    WaitSecs(3);
    while true
        % Generate a random integer from 1 - 100
        roll = ceil(rand * 100);
        
        % Pause for a short moment so we can see the rolling on the screen
        pause(0.05);
        
        % Tell what you rolled:
        fprintf('I rolled a: %d\n', roll);
        
        % If we roll 100, be happy and then leave the loop.
        if roll == 100
            fprintf('JACKPOT, 100!, we can stop now\n');
            
            % Use break to get out of the while-loop
            break
        end
    end
    fprintf('We made it out of the loop succesfully, phew!\n')
    
end


