function for_loops
%function for_loops
%
% A brief explanation on for loops
%

    % A for loop is a way of repeating certain commands a number of
    % times. The common syntax of a for loop is the following:
    %
    % for i = [some vector]
    %    do something;
    %    do something else;
    % end
    %
    % In this way, the statements inside the loop are repeated as many times
    % as there are entries in 'some vector'. Each time the loop is repeated,
    % the value of 'i' becomes the next entry in the vector. So the first
    % time i equals the first value in the vector, the second time the
    % second, the third time the third, etc.


    next_section('for loop 1');
    % For example, we already saw that we can make lists of numbers using
    % colon notation like this: x = 1 : 10, making all the numbers from 1 to
    % 10. We could use this in a for-loop like this:
    for i = 1 : 5
        fprintf('The value of i is now: %d\n', i);
    end


    next_section('for loop 2');
    % We can also loop through an existing list like this:
    x = [2 3 5 8];
    for i = x
        fprintf('The value of i is now: %d\n', i);
    end


    next_section('for loop 3');
    % A common thing we want to do is loop through all the entries in a
    % list. If that list is a vector, then we can use the syntax
    % above. However, if x is a cell-array then we can always do this:
    x = {'this', 'is', 'another', 'example'};
    for i = 1 : length(x)
        fprintf('The value of x{%d} is: %s\n', i, x{i});
    end

end
