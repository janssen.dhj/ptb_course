function generating_a_random_vector
%function generating_a_random_vector
%
% When we run an experiment, we often want to include an element of
% randomness, like the location in which we display the stimulus, or the
% interval in which the stimulus occurs.
%

    %% Generating a random binary vector
    % Often we have a binary element in our randomness. I.e., the image is
    % shown on the left or on the right or the image occurs in the first
    % interval or the second. To do this we can use the the matlab function
    % 'rand' which draws random numbers from a uniform distribution in the
    % range [0, 1].
        
    % Generate a vector of random numbers
    n_entries = 40;                % The amount of random numbers we want
    rand_vec = rand(n_entries, 1); % Generate the a vector with n_entries
    
    % The numbers we just generated are distributed between 0 and 1,
    % however, we want binary values. Since the distribution from which we
    % draw is uniform, we can easily convert it to a binary form where both
    % options are equally likely by comparing the vector to 0.5. Any entry
    % where the value is above 0.5 will become 'True' or '1', and all the
    % others become 'False' or '0'.
    
    % Generate binary vector with equal probabilities.
    binary_vec = rand_vec > 0.5;
    
    % Of course, if we want only a 30% chance pof True, and a 70% chance of
    % false, we can construct the vector like this:
    binary_vec = rand_vec < 0.3;
    size(binary_vec)
    
    %% Using the entries in the binary vector to make a random decision
    % Now we can use the 'true' and 'false' boolean values in our binary_vec
    % to make some decisions in our experiment. Here we only write to the
    % Matlab prompt, but usually you use the values in your random vector to
    % decide where to display an image, or which image to display.
    %
    % If you do not know how 'if' works, check the 'if_statement.m' example.
    
    % We can loop through our binary_vec, and whenever the entry is True, we
    % do thing A, and whenever the entry if False, we do thing B.
    A_things = 0;
    B_things = 0;
    for i = 1 : length(binary_vec)
        % Now we compare decision_var to 'true'. If decision_var is true, we
        % do the first statement after the 'if', otherwise we do the
        % statement after the 'else'.
        %
        % NOTE: since decision_var is already a boolean, comparing it to
        % true is actually not necessary, we could just as well have
        % written:
        %
        % if decision_var
        %    DO_THING_A
        % else
        %    DO_THING_B
        % end
        %
        % But I include the ' == true' here for clarity.
        decision_var = binary_vec(i);
        if decision_var == true
            fprintf('We are doing thing A!\n')
            A_things = A_things + 1;
        else
            fprintf('We are doing thing B!\n')
            B_things = B_things + 1;
        end
    end
    
    fprintf('We did %d A things!\n', A_things)
    fprintf('We did %d B things!\n', B_things)
end
