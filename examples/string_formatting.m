function string_formatting
%function string_formatting
%
% A small demonstration of string formatting with sprintf and fprintf
%
    
    %% Intro:
    % Sometimes you have a standard string you want to print out, but you
    % want to insert some numbers or another string inside at a certain
    % point. You could build the string manually using str2num and vector
    % concatenation operations like this:
        
    next_section('annoying way of formatting')
    s = ['My age is: ' num2str(28) ' and pi equals ' num2str(pi)];
    disp(s);
    
    % However, this can become annoying and difficult as the string gets
    % bigger. There are better ways. For example, we can use the sprintf
    % command. It takes a string with 'special characters' and inserts
    % values at those special characters. There are many different special
    % ones, and for a full review you should look at the help and doc of
    % sprintf, but here we will introduce 3:
    % '%d': insert a literal number, integers insert as integers, while
    %       floats insert as scientific notation
    % '%f': insert something as a floating number (not scientific notation)
    % '%s': insert a string
    %
    
    % for example:
    
    next_section('sprintf examples');
    
    disp(sprintf('pi equals: %f', pi));
    disp(sprintf('pi equals: %d', pi));
    disp(sprintf('%s and %s are the names of Odins ravens', ...
                 'Hugin', 'Munin'));
    
    % We can also easily loop through things:
    hobbit_dwarf_names = {'Balin', 'Bifur', 'Bofur', 'Bombur', 'Dori', 'Dwalin', ...
                        'Fili', 'Gloin', 'Kili','Nori', 'Oin', 'Ori'};
    fmt_s = 'Dwarf number %d is called: %s';
    for i = 1 : length(hobbit_dwarf_names)
        disp(sprintf(fmt_s, i, hobbit_dwarf_names{i}));
    end
    
    %% Special characters
    % Sometimes you want to insert 'special characters' in format-strings,
    % like 'new-lines' (enters) or 'tabs'. We do this by using
    % 'escape-characters'. Again, there are many, but here we cover 2:
    % \n - newline, i.e., like pushing enter
    % \t - tab, i.e., outlines with tabs
       
    next_section('special characters');
    s = sprintf('My name is %s\nMy name is %s\nMy name is %s', ...
                'What!', 'Who!', 'Slim shady');
    disp(s);
    
    disp('Without using sprintf:');
    s = ['Look\n\tThis\n\t\tText\n\t\t\tLooks\n\t\t\t\tLike\n\t\t\t\t\' ...
         'tStairs!'];
    disp(s)
    
    disp('With sprintf:');
    disp(sprintf(s));
    
    %% fprintf
    % Just a quick mention: we've been using fprintf a lot already in these
    % demos to write things to the screen. It's used basically as sprintf,
    % except that it can write to files. So a command like:
    % fid = fopen('hello.txt', 'w')
    % fprintf(fid, 'My name is %s', 'David')
    % fclose(fid);
    % Would create a text-file and write "My name is David" to that
    % text-file.
    %
    % However, the only way we use it is to write directly to the screen. If
    % you don't give a file-identifier, it uses the matlab output as the
    % 'file'. So instead of writing disp(sprintf(...)) we just write
    % fprintf(...)
    
    % You have to add a \n to the end of your line, it does *not* insert if
    % automatically.
    
    next_section('fprintf');
    fprintf('I write to the screen!')
    fprintf('But I forgot to but an newline!!\n')
    
    fmt_s = 'The product of the numbers from %d to %d is %d\n';
    for i = 1 : 10
        fprintf(fmt_s, i, 10, prod(i : 10));
    end
    
        
    
end
