function next_section(name)
% Little helper function that separates the output on the command window
% into distinct blocks. Feel free to ignore this function, you don't
% need to understand it.
    w = 78;
    len_A = floor((w - length(name)) / 2);
    len_B = ceil((w - length(name)) / 2);
    s = ['\n' repmat('-', 1, len_A) name repmat('-', 1, len_B) '\n\n'];
    fprintf(s);
end

