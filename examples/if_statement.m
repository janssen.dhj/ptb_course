function if_statement
%function if_statement
%
% A demonstration of if-statements
%

    
    %% Basic if-statements
    % You can use an if-statement to do something if and only if a certain
    % condition is met. The syntax for if-statements is:
    %
    % if SOME_KIND_OF_TEST
    %     do something
    % end
    %
    %
    
    next_section('basic if-statements')
    % Since 1 is *not* larger than 2, the code after the if statement does
    % *not* happen.
    if 1 > 2
        fprinft('This will never happen\n')
    end
    
    % Since 2 is usually larger than 1, the code after this if statement
    % does happen.
    if 2 > 1
        fprintf('This does happen!\n')
    end
    
    % Try the '2 > 1' code at your matlab command prompt, what does it show?
    % Try the '1 > 2' code at your matlab command prompt, what does it show?
    
    % The statement after the 'if' always has to achieve some kind of
    % true/false status. Most kinds of numbers are 'true', except for 0.
    % 'if NaN' will actually throw an error because Matlab does not know if
    % NaN is 'true' or 'false'. 'Inf', however, is true.
    
    if true
        fprintf('Yep, this prints\n')
    end
    
    if false
        fprintf('Nope, this does not print\n')
    end
    
    if 5
        fprintf('Yep, 5 is true, this prints\n')
    end
    
    % if NaN
    %     fprinft('Uncommenting this actually breaks the code!\n')
    % end
    
    if Inf
        fprintf('Inf is true, so this prints\n')
    end
    
    %% If-else statements
    % Instead of using the 'if' statement to decide whether we should or
    % should not do 'thing A', we can also use it to decide if we should 'do
    % thing A or thing B'. For this we use the else statement. If looks like
    % this:
    %
    % if SOMETHING
    %    do Thing A
    % else
    %    do Thing B
    % end
    %
    % If 'SOMETHING' is true, we will do Thing A, if it is *not* true, we
    % will do Thing B instead
    %
    next_section('if-else statements')
    % If SOMETHING is false, we will skip the code in the first block, and
    % do the code in the second block.
    if 1 > 2
        % This will not happen
        fprintf('1 is larger than 2\n')
    else
        % This will happen
        fprintf('1 is *not* larger than 2\n')
    end
        
    % If SOMETHING is true, we will do the first block of code, otherwise we
    % will do the second bit.
    if true
        % This will happen
        fprintf('true is true\n');
    else
        % This will not happen
        fprintf('true is false\n');
    end
    
    %% if-ifelse-else statements
    % We can make a whole series of tests to decide what we should do using
    % the if-ifelse-else construct. It will keep trying the tests inside the
    % if-ifelse blocks. The moment it encounters 1 that is True, it will do
    % that bit of code, and then leave the construct. If nothing is found
    % that is true, it will do the 'else'
    %
    % The syntax is:
    % if SOME TEST A
    %     do Thing A
    % elseif SOME TEST B
    %     do Thing B
    % elseif SOME TEST C
    %     do Thing C
    % ...
    % ...
    % elseif SOME TEST Q
    %     do Thing Q
    % else
    %     do THing R
    % end
    %
    next_section('if-elseif-else statements')

    
    % The first two tests are both 'false', so we will do the last block of
    % code.
    if 1 > 1
        fprintf('1 is larger than 1\n');
    elseif 1 < 1
        fprintf('1 is smaller than 1\n');
    else
        fprintf('1 is not smaller or larger than 1\n')
    end
    
    % The 2nd test is true, and although the 3d test is *also* true, we
    % won't do it, because we find the first thing that is true, do it, and
    % then leave the if block.
    if isstruct('hello')
        fprintf('the string "hello" is also a struct!\n')
    elseif 3^2 == 9
        fprintf('3 squared is nine!\n')
    elseif 3^3 == 27
        fprintf('3 cubed is twenty-seven!\n')
    else
        fprintf('Nothing I could find was true :-(');
    end
    
    % We can leave the 'else' out, if we find something that is 'true' we
    % will still do it, but if nothing is true, and no 'else' was found, we
    % just don't do anything at all. This block does nothing:
    if 3 == 2
        fprintf('3 equals 2!\n')
    elseif 3 == 4
        fprintf('3 equals 4!\n')
    end
end


