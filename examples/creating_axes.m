function creating_axes
%function creating_axes
%

% When we create an image, we usually start by creating a grid. And we
% usually start creating a grid by creating axes.

% Create axes using 'from:step:to':
% In matlab we can create lists of numbers using the colon notation. It
% creates a list (vector) of numbers. The first number is 'from'. The second
% number if 'from + step', the third number is 'from + 2 * step' etc. The
% list stops with the last number below 'to'. If the 'step' argument is
% omitted, it is assumed to be 1'
    next_section('colons')
    fprintf('0:1:5 creates:\n')
    0:1:5
    fprintf('0:5 also creates:\n')
    0:5
    fprintf('0:0.5:5 creates:\n')
    0:0.5:5
    fprintf('10:-2:0 creates:\n')
    10:-2:0
    fprintf('1:3:8 creates:\n')
    fprintf('(Note: the last number is *not* 8)')
    1:3:8
    
% Create axes using 'linspace':
% The 'linspace' function in matlab takes 3 arguments: 'from', 'to',
% 'amount'. Then it returns a list (vector) of numbers starting at 'from'
% and ending with 'to'. The vector contains exactly 'amount' numbers, and
% they are all equally far from eachother.
    next_section('linspace')
    fprintf('linspace(0, 4, 5) creates:\n')
    linspace(0, 4, 5)
    fprintf('linspace(0, 1, 5) creates:\n')
    linspace(0, 1, 5)
    fprintf('linspace(-pi, pi, 20) creates:\n')
    linspace(-pi, pi, 20)
    
    % ASSIGNMENT:
    % use linspace, create a vector 'x' with 5 numbers between 0 and 10
    % make a vector 'y' that equals: exp(x)
    % call: plot(x, y) and see what happens.
    % Now redo the above 3 steps, but first with 10 numbers, then 100 numbers
    % (If you are lost, the solution is at the bottom of this file)
end



























function solution
    x = linspace(0, 10, 5);
    y = exp(x);
    plot(x, y);
end
